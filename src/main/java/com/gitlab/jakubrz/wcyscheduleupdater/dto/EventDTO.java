package com.gitlab.jakubrz.wcyscheduleupdater.dto;

import com.gitlab.jakubrz.wcyscheduleupdater.Utils;
import com.google.api.services.calendar.model.Event;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.Optional;
import java.util.function.Function;

@Getter
@Builder
public class EventDTO {
    private String summary;
    private String description;
    private String timeRange;
    private String location;
    private String oldSummary;
    private String oldDescription;
    private String oldTimeRange;
    private String oldLocation;

    public static EventDTO createFrom(Event event) {
        return EventDTO.builder()
                .summary(event.getSummary())
                .description(event.getDescription())
                .location(event.getLocation())
                .timeRange(Utils.getTimeRange(event))
                .build();
    }

    public static EventDTO createFrom(Event event, Event old) {
        VersionComparator comparator = new VersionComparator(old, event);
        return EventDTO.builder()
                .summary(event.getSummary())
                .description(event.getDescription())
                .location(event.getLocation())
                .timeRange(Utils.getTimeRange(event))
                .oldSummary(comparator.getOld(Event::getSummary).orElse(null))
                .oldDescription(comparator.getOld(Event::getDescription).orElse(null))
                .oldLocation(comparator.getOld(Event::getLocation).orElse(null))
                .oldTimeRange(comparator.getOld(Utils::getTimeRange).orElse(null))
                .build();
    }

    @AllArgsConstructor
    private static class VersionComparator {
        private Event before;
        private Event after;

        private Optional<String> getOld(Function<Event, String> mapper) {
            return getOld(mapper.apply(before), mapper.apply(after));
        }

        private static Optional<String> getOld(String before, String after) {
            return before.equals(after) ? Optional.empty() : Optional.of(before);
        }
    }

}
