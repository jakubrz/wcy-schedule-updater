package com.gitlab.jakubrz.wcyscheduleupdater.dto;

import com.gitlab.jakubrz.wcyscheduleupdater.Utils;
import com.google.api.client.util.Key;
import lombok.Getter;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.stream.Stream;

import static com.gitlab.jakubrz.wcyscheduleupdater.Constants.CONFIG_FILE_PATH;
import static com.gitlab.jakubrz.wcyscheduleupdater.Constants.JSON_FACTORY;

@Getter
public class ConfigDTO {

    @Key("login")
    private String login;

    @Key("password")
    private String password;

    @Key("group")
    private String group;

    @Key("mid")
    private String mid;

    @Key("iid")
    private String iid;

    @Key("base_url")
    private String baseUrl;

    @Key("year")
    private String year;

    @Key("lectures_calendar")
    private String lecturesCalendar;

    @Key("others_calendar")
    private String othersCalendar;

    @Key("notification_email")
    private String notificationEmail;

    @Key("url")
    private String url;

    public final static ConfigDTO config;

    static {
        try {
            config = loadConfig();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static ConfigDTO loadConfig() throws IOException {
        InputStream resource = new FileInputStream(CONFIG_FILE_PATH);
        ConfigDTO config = JSON_FACTORY.createJsonParser(resource).parseAndClose(ConfigDTO.class);
        String url = config.getUrl();
        if (url != null && !url.isEmpty()) {
            String baseUrl = url.substring(0, url.lastIndexOf("/") + 1);
            if (baseUrl.isEmpty()) {
                throw new IllegalStateException();
            }
            Map<String, String> params = Utils.getUrlParams(url);
            Stream.of("exv", "iid", "mid").forEach(key -> {
                if (!params.containsKey(key)) {
                    throw new IllegalStateException();
                }
            });
            config.baseUrl = baseUrl;
            config.group = params.get("exv");
            config.iid = params.get("iid");
            config.mid = params.get("mid");
        }
        return config;
    }

}
