package com.gitlab.jakubrz.wcyscheduleupdater;

import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toMap;

public class Utils {
    public static final DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public static final DateFormat timeFormat = new SimpleDateFormat("HH:mm");
    public static final Pattern EVENT_ID_PATTERN = Pattern.compile("(.*)(-.*)(\\(.*)");

    public static EventDateTime getEventDateTime(String date, String time) {
        return new EventDateTime().setDateTime(new DateTime(parseDateTime(date, time)));
    }

    public static Date parseDateTime(String date, String time) {
        try {
            return dateTimeFormat.parse(date + " " + time);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getTimeRange(Event event) {
        Date start = getDateTime(event.getStart());
        Date end = getDateTime(event.getEnd());
        return dateTimeFormat.format(start)
                + " - "
                + (isSameDay(start, end) ? timeFormat.format(end) : dateTimeFormat.format(end));
    }

    public static Date getDateTime(EventDateTime eventDateTime) {
        DateTime dateTime = eventDateTime.getDateTime() != null ? eventDateTime.getDateTime() : eventDateTime.getDate();
        return new Date(dateTime.getValue());
    }

    public static boolean isSameDay(Date date1, Date date2) {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        return fmt.format(date1).equals(fmt.format(date2));
    }

    public static InputStream getLoggingPropertiesStream() {
        return Utils.class.getClassLoader().getResourceAsStream("logging.properties");
    }


    public static Map<String, String> getUrlParams(String url) {
        String[] params = url.substring(url.indexOf("?") + 1).split("&");
        return stream(params)
                .map(x -> x.split("="))
                .filter(x -> x.length == 2)
                .collect(toMap(x -> x[0], x -> x[1]));
    }

    public static String getEventId(Event event) {
        Matcher matcher = EVENT_ID_PATTERN.matcher(event.getDescription());
        if (matcher.find()) {
            return matcher.group(1) + matcher.group(3);
        }
        throw new IllegalStateException("Couldn't parse event ID");
    }
}
