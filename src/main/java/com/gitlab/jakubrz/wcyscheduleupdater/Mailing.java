package com.gitlab.jakubrz.wcyscheduleupdater;

import com.google.api.client.util.Base64;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import com.gitlab.jakubrz.wcyscheduleupdater.dto.ConfigDTO;
import com.gitlab.jakubrz.wcyscheduleupdater.dto.EventDTO;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.function.Consumer;
import java.util.logging.Logger;

import static java.util.Collections.singletonList;
import static javax.mail.Message.RecipientType.TO;
import static com.gitlab.jakubrz.wcyscheduleupdater.Auth.auth;

public class Mailing {
    private final Logger logger = Logger.getGlobal();
    private Gmail gmail;

    public Mailing() {
        gmail = auth.buildClientService(Gmail.Builder::new);
    }

    public static Mailing create() {
        return new Mailing();
    }

    public void send(String html) {
        try {
            List<String> contacts = singletonList(ConfigDTO.config.getNotificationEmail());
            String subject = "Zmiany w planie zajęć";
            sendMessage(createEmail(contacts, subject, html));
        } catch (MessagingException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void send(List<EventDTO> deleted,
                     List<EventDTO> inserted,
                     List<EventDTO> updated) {
        logger.info("Preparing email");
        send(buildEmailFromTemplate(deleted, inserted, updated));
    }

    private String buildEmailFromTemplate(List<EventDTO> deleted,
                                          List<EventDTO> inserted,
                                          List<EventDTO> updated) {
        Map<String, Object> model = new HashMap<>();
        model.put("deleted", deleted);
        model.put("inserted", inserted);
        model.put("updated", updated);
        try {
            logger.info("Building email from template");
            Writer contentWriter = new StringWriter();
            Configuration cfg = new Configuration();

            cfg.setDefaultEncoding("UTF-8");
            cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

            cfg
                    .getTemplate("email-template.ftl")
                    .process(model, contentWriter);
            return contentWriter.toString();
        } catch (IOException | TemplateException e) {
            throw new RuntimeException(e);
        }

    }

    private static Message convertMessage(MimeMessage emailContent)
            throws IOException, MessagingException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        emailContent.writeTo(buffer);
        byte[] bytes = buffer.toByteArray();
        String encodedEmail = Base64.encodeBase64URLSafeString(bytes);
        Message message = new Message();
        message.setRaw(encodedEmail);
        return message;
    }

    private Message sendMessage(MimeMessage emailContent) throws MessagingException, IOException {
        logger.info("Sending email");
        Message message = convertMessage(emailContent);
        message = gmail.users().messages().send("me", message).execute();
        logger.info("Message id: " + message.getId());
        logger.info(message.toPrettyString());
        return message;
    }

    private static MimeMessage createEmail(List<String> recipients,
                                           String subject,
                                           String html)
            throws MessagingException {
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        MimeMessage email = new MimeMessage(session);
        recipients.forEach(addRecipient(email));
        email.setSubject(subject, "UTF-8");
        email.setContent(html, "text/html; charset=UTF-8");
        return email;
    }

    private static Consumer<String> addRecipient(MimeMessage email) {
        return address -> {
            try {
                email.addRecipient(TO, new InternetAddress(address));
            } catch (MessagingException e) {
                throw new RuntimeException(e);
            }
        };
    }
}
