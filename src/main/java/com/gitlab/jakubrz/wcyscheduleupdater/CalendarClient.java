package com.gitlab.jakubrz.wcyscheduleupdater;

import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.gitlab.jakubrz.wcyscheduleupdater.Constants.MAX_RESULTS;

public class CalendarClient {
    private final Calendar service;

    public CalendarClient(Calendar service) {
        this.service = service;
    }

    public Calendar.Events.Insert insertEvent(Event event, String calendarId) {
        try {
            return service.events().insert(calendarId, event);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Calendar.Events.Update updateEvent(Event event, String eventId, String calendarId) {
        try {
            return service.events().update(calendarId, eventId, event);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Calendar.Events.Delete deleteEvent(String calendarId, Event event) {
        try {
            return service.events().delete(calendarId, event.getId());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Event> listAllEvents(String id) {
        List<Event> items = new ArrayList<>();
        Events events = null;
        do {
            String pageToken = events != null ? events.getNextPageToken() : null;
            events = execute(
                    listEvents(id).setMaxResults(MAX_RESULTS)
                            .setPageToken(pageToken)
            );
            items.addAll(events.getItems());
        } while (events.getNextPageToken() != null);
        return items;
    }

    public Calendar.Events.List listEvents(String id) {
        try {
            return service.events().list(id);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public <T> T execute(AbstractGoogleClientRequest<T> request) {
        try {
            return request.execute();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
