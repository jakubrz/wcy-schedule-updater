package com.gitlab.jakubrz.wcyscheduleupdater;

import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.gmail.GmailScopes;

import java.util.List;

import static java.util.Arrays.asList;

public class Constants {
    public static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    public static final String CREDENTIALS_FILE_PATH = "/credentials.json";
    public static final String TOKENS_DIRECTORY_PATH = "tokens";
    public static final String APPLICATION_NAME = "WCY Schedule Updater";
    public static final Integer MAX_RESULTS = 250;
    public static final String CELL_QUERY = ".tdFormList1DSheTeaGrpHTM3";
    public static final String USER_AGENT = "Chrome";
    public static final String CONFIG_FILE_PATH = "config.json";
    public static final String LOGGED_IN_SIGNATURE = "ed/logged_in";
    public static final String YEAR_SEPARATOR = "/";
    public static final String LECTURE_SIGNATURE = "(Wykład)";
    public static final List<String> SCOPES = asList(
            GmailScopes.GMAIL_SEND,
            CalendarScopes.CALENDAR_EVENTS
    );
}
