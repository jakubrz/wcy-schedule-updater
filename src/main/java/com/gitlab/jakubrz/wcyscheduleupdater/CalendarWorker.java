package com.gitlab.jakubrz.wcyscheduleupdater;

import com.gitlab.jakubrz.wcyscheduleupdater.dto.ConfigDTO;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.gitlab.jakubrz.wcyscheduleupdater.dto.EventDTO;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Logger;

import static java.util.stream.Collectors.toList;

public class CalendarWorker {
    private static final Logger logger = Logger.getGlobal();

    private GoogleClientBatchExecutor batch;
    private CalendarClient calendarClient;

    public CalendarWorker() {
        Calendar calendarService = Auth.auth.buildClientService(Calendar.Builder::new);
        calendarClient = new CalendarClient(calendarService);
        batch = new GoogleClientBatchExecutor(calendarService);
    }

    public void makeChangesAgile(List<Event> fetchedEvents) throws IOException {
        Map<String, List<Event>> calendars = getCalendars(ConfigDTO.config.getOthersCalendar(), ConfigDTO.config.getLecturesCalendar());

        logger.info("Detecting changes");
        List<EventDTO> deleted = handleDeletes(fetchedEvents, calendars);
        List<EventDTO> inserted = handleInserts(fetchedEvents, calendars);
        List<EventDTO> updated = handleUpdates(fetchedEvents, calendars);

        if (!deleted.isEmpty() || !inserted.isEmpty() || !updated.isEmpty()) {
            Mailing.create().send(deleted, inserted, updated);
        }
        logger.info("All events up to date");
    }

    private List<EventDTO> handleDeletes(List<Event> fetchedEvents, Map<String, List<Event>> calendars) {
        List<EventDTO> deleted = new ArrayList<>();
        List<Calendar.Events.Delete> deletes = calendars.entrySet().stream().flatMap(entry -> {
                    String cid = entry.getKey();
                    List<Event> events = entry.getValue();
                    return events.stream()
                            .filter(containsById(fetchedEvents).negate())
                            .peek(e -> deleted.add(EventDTO.createFrom(e)))
                            .map(e -> calendarClient.deleteEvent(cid, e));
                }
        ).collect(toList());
        batch.execute(deletes, Calendar.Events.Delete::getCalendarId);
        return deleted;
    }

    private List<EventDTO> handleInserts(List<Event> fetchedEvents, Map<String, List<Event>> calendars) {
        List<EventDTO> inserted = new ArrayList<>();
        List<Calendar.Events.Insert> inserts = fetchedEvents.stream()
                .filter(containsById(calendars).negate())
                .peek(e -> inserted.add(EventDTO.createFrom(e)))
                .map(event -> calendarClient.insertEvent(event, getCalendarId(event)))
                .collect(toList());
        batch.execute(inserts, Calendar.Events.Insert::getCalendarId);
        return inserted;
    }

    private List<EventDTO> handleUpdates(List<Event> fetchedEvents, Map<String, List<Event>> calendars) {
        List<EventDTO> updated = new ArrayList<>();
        List<Calendar.Events.Update> updates = fetchedEvents.stream()
                .filter(event -> !containsExactly(calendars.get(getCalendarId(event)), event))
                .map(event ->
                        calendars.
                                get(getCalendarId(event))
                                .stream()
                                .filter(x -> equals(x, event, Utils::getEventId))
                                .peek(oldEvent -> updated.add(EventDTO.createFrom(event, oldEvent)))
                                .findFirst()
                                .map(Event::getId)
                                .map(id -> calendarClient.updateEvent(event, id, getCalendarId(event)))
                                .orElse(null)
                ).filter(Objects::nonNull)
                .collect(toList());
        batch.execute(updates, Calendar.Events.Update::getCalendarId);
        return updated;
    }

    private Predicate<Event> containsById(List<Event> events) {
        return event -> events.stream().anyMatch(e -> equals(e, event, Utils::getEventId));
    }

    private Predicate<Event> containsById(Map<String, List<Event>> calendars) {
        return event -> containsById(calendars.get(getCalendarId(event))).test(event);
    }

    private static boolean containsExactly(List<Event> events, Event event) {
        return events.stream().anyMatch(e -> equals(e, event));
    }

    private static boolean equals(Event e1, Event e2) {
        return equals(e1, e2, Event::getSummary)
                && equals(e1, e2, Event::getDescription)
                && equals(e1, e2, Event::getStart)
                && equals(e1, e2, Event::getEnd)
                && equals(e1, e2, Event::getLocation);
    }

    private static boolean equals(Event e1, Event e2, Function<Event, Object> mapper) {
        Object v1 = mapper.apply(e1);
        Object v2 = mapper.apply(e2);
        return Objects.equals(v1, v2);
    }

    private Map<String, List<Event>> getCalendars(String... ids) throws IOException {
        Map<String, List<Event>> calendars = new HashMap<>();
        for (String id : ids) {
            List<Event> items = calendarClient.listAllEvents(id);
            calendars.put(id, items);
            logger.fine(items.size() + " events fetched from calendar: " + id);
        }
        return calendars;
    }


    private String getCalendarId(Event event) {
        return event.getDescription().contains(Constants.LECTURE_SIGNATURE) ? ConfigDTO.config.getLecturesCalendar() : ConfigDTO.config.getOthersCalendar();
    }


    private void clearCalendar(Calendar service, String calendarId) throws IOException {
        List<Calendar.Events.Delete> deletes = service.events().list(calendarId).execute()
                .getItems()
                .stream()
                .map(event -> calendarClient.deleteEvent(calendarId, event))
                .collect(toList());
        batch.execute(deletes);
    }

}
