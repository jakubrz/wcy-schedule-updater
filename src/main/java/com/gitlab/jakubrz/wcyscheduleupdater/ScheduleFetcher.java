package com.gitlab.jakubrz.wcyscheduleupdater;

import com.gitlab.jakubrz.wcyscheduleupdater.dto.ConfigDTO;
import com.google.api.services.calendar.model.Event;
import com.google.common.collect.Iterables;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ScheduleFetcher {
    private static final Logger logger = Logger.getGlobal();
    private Set<String> abbreviations = new TreeSet<>();

    public List<Event> fetchEvents() throws IOException {
        String sid = authenticate();
        Document document = fetchDocument(sid);
        return getEvents(document);
    }

    private List<Event> getEvents(Document document) {
        Elements cells = document.body().select(Constants.CELL_QUERY);

        List<Event> events = cells.stream().filter(cell -> !cell.text().trim().isEmpty())
                .map(this::getEvent)
                .collect(Collectors.toList());

        Map<String, String> map = new Hashtable<>();
        AtomicInteger i = new AtomicInteger(0);
        abbreviations.forEach(x -> map.put(x, String.valueOf(i.incrementAndGet())));
        events.forEach(e -> e.setColorId(map.get(e.get("abbreviation").toString())));

        checkEvents(events);
        return events;
    }

    private void checkEvents(List<Event> events) {
        if (events.isEmpty()) {
            throw new IllegalStateException("No events from schedule!");
        }
    }

    private Document fetchDocument(String sid) throws IOException {
        logger.fine("fetching document");
        String planUrl = ConfigDTO.config.getBaseUrl() + "logged_inc.php?" + "sid=" + sid + "&mid=" + ConfigDTO.config.getMid() + "&iid=" + ConfigDTO.config.getIid() + "&exv=" + ConfigDTO.config.getGroup();
        Document document = connect(planUrl).get();
        checkAuthentication(document);
        logger.info("document fetched");
        return document;
    }

    private Event getEvent(Element cell) {
        Elements nobrs = cell.select("nobr");
        String abbreviation = nobrs.get(0).child(0).text();
        String type = nobrs.get(0).child(2).text();
        String location = Iterables.getLast(nobrs.get(0).textNodes()).text();
        String teacherAbbreviation = nobrs.get(1).text();
        String counter = nobrs.get(2).text();
        String title = cell.attr("title");
        String date = getDate(cell);
        String time[] = getTime(cell);
        String startTime = time[0];
        String endTime = time[1];

        Event event = new Event();
        event.setSummary(cell.text());
        event.setLocation(location);
        event.setDescription(title + " " + counter);
        event.setStart(Utils.getEventDateTime(date, startTime));
        event.setEnd(Utils.getEventDateTime(date, endTime));
        event.set("abbreviation", abbreviation);
        abbreviations.add(abbreviation);
        return event;
    }

    private String[] getTime(Element cell) {
        return cell.parent().child(1).text().split(" ");
    }

    private String getDate(Element cell) {
        int parentIndex = cell.parent().siblingIndex();
        int cellIndex = cell.siblingIndex();
        String pseudoDate[] = cell.parent().parent().child(parentIndex - parentIndex % 8).child(cellIndex + 1).text().split(" ");
        return convertMonth(pseudoDate[1]) + pseudoDate[0];
    }

    private String convertMonth(String romanMonth) {
        int month = RomanNumeralMonths.toArabic(romanMonth);
        String year = ConfigDTO.config.getYear();
        if (year.contains(Constants.YEAR_SEPARATOR)) {
            year = year.split(Constants.YEAR_SEPARATOR)[month <= 8 ? 1 : 0];
        }
        return year + "-" + month + "-";
    }

    private String authenticate() throws IOException {
        logger.fine("authenticating");
        String action = connect(ConfigDTO.config.getBaseUrl()).get().selectFirst("form").attr("action");
        String sid = action.substring(action.indexOf("=") + 1);

        Document postDoc = connect(ConfigDTO.config.getBaseUrl() + action)
                .data("userid", ConfigDTO.config.getLogin())
                .data("password", ConfigDTO.config.getPassword())
                .data("default_fun", "1")
                .data("formname", "login")
                .post();

        checkAuthentication(postDoc);
        logger.info("authenticated");
        return sid;
    }

    private void checkAuthentication(Document document) {
        if (!document.location().contains(Constants.LOGGED_IN_SIGNATURE)) {
            throw new IllegalStateException("Authentication failed!");
        }
    }

    private Connection connect(String s) {
        return Jsoup
                .connect(s)
                .userAgent(Constants.USER_AGENT)
                .validateTLSCertificates(false);
    }
}
