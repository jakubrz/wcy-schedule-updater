package com.gitlab.jakubrz.wcyscheduleupdater;

import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.googleapis.services.AbstractGoogleClient;
import com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest;
import com.google.api.client.http.HttpHeaders;
import com.google.common.collect.Lists;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Logger;

import static java.util.stream.Collectors.groupingBy;

public class GoogleClientBatchExecutor {
    private static final Logger logger = Logger.getGlobal();

    private final AbstractGoogleClient service;

    public GoogleClientBatchExecutor(AbstractGoogleClient service) {
        this.service = service;
    }

    private static <T> JsonBatchCallback<T> createCallback() {
        return new JsonBatchCallback<T>() {
            @Override
            public void onFailure(GoogleJsonError googleJsonError, HttpHeaders httpHeaders) {
                logger.severe("CalendarWorker.onFailure googleJsonError = [" + googleJsonError + "], httpHeaders = [" + httpHeaders + "]");
            }

            @Override
            public void onSuccess(T value, HttpHeaders httpHeaders) {
                logger.fine("CalendarWorker.onSuccess, value = [" + value + "], httpHeaders = [" + httpHeaders + "]");
            }
        };
    }

    public <T, U extends AbstractGoogleJsonClientRequest<T>> void execute(List<U> requests, Function<U, Object> classifier) {
        requests.stream().collect(groupingBy(classifier)).values().forEach(this::execute);
    }

    public <T, U extends AbstractGoogleJsonClientRequest<T>> void execute(List<U> requests) {
        JsonBatchCallback<T> callback = createCallback();
        Lists.partition(requests, 50).forEach(chunk -> {
            BatchRequest batch = service.batch();
            chunk.forEach(queue(batch, callback));
            execute(batch);
        });
    }

    private static void execute(BatchRequest batch) {
        try {
            batch.execute();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static <T> Consumer<AbstractGoogleJsonClientRequest<T>> queue(BatchRequest batch, JsonBatchCallback<T> callback) {
        return request -> {
            try {
                request.queue(batch, callback);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        };
    }
}
