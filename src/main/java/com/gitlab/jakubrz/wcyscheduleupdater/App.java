package com.gitlab.jakubrz.wcyscheduleupdater;

import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.logging.LogManager.getLogManager;
import static com.gitlab.jakubrz.wcyscheduleupdater.Utils.getLoggingPropertiesStream;

public class App {
    public static void main(String[] args) {
        try {
            getLogManager().readConfiguration(getLoggingPropertiesStream());
            new CalendarWorker().makeChangesAgile(
                    new ScheduleFetcher().fetchEvents()
            );
        } catch (Throwable e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            System.exit(1);
        }
    }

}
