<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Zmiany w planie</title>
    <style>
        html, body {
            background-color: #ddd;
            font-family: Calibri, "Helvetica Neue", sans-serif;
        }

        .container {
            background-color: #ddd;
            font-family: Calibri, "Helvetica Neue", sans-serif;
            font-size: 1.1em;
        }

        .content {
            max-width: 600px;
            min-width: 250px;
            margin: 0 auto;
            border-color: lightgray;
            border-width: 0 1px;
            border-style: solid;
        }

        .header {
            background-color: #3f51b5;
            color: white;
            padding: 18px 0 6px;
            text-align: center;
        }

        .footer {
            background-color: gray;
            color: white;
            padding: 12px;
            font-size: 0.9em;
            text-align: center;
            margin-bottom: 24px;
        }

        .main {
            background-color: white;
            padding: 12px;
        }

        .event-box {
            display: block;
            padding: 5px;
            margin: 5px;
        }

        .deleted {
            background: mistyrose;
        }

        .inserted {
            background: honeydew;
        }

        .updated {
            background: aliceblue;
        }

        .change {
            background: lightsteelblue;
            border-radius: 5px;
            padding: 1px;
        }

        .event-header {
            font-weight: bold;
        }

        .event-property {
            margin-left: 10px;
        }
    </style>
<body>
<div class="container">
    <div class="content">
        <div class="header">
            <h2>Wykryto zmiany w planie zajęć</h2>
        </div>
        <div class="main">

        <#if deleted?has_content>
            <h1>Usunięte</h1>
        </#if>
<#list deleted as event>
            <div class="event-box deleted">
                <div class="event-header">
                    ${event.summary}
                </div>
                <div class="event-property">
                    ${event.description}
                </div>
                <div class="event-property">
                    ${event.timeRange}
                </div>
                <div class="event-property">
                    ${event.location}
                </div>
            </div>
</#list>

<#if inserted?has_content>
            <h1>Dodane</h1>
</#if>
<#list inserted as event>
            <div class="event-box inserted">
                <div class="event-header">
                    ${event.summary}
                </div>
                <div class="event-property">
                    ${event.description}
                </div>
                <div class="event-property">
                    ${event.timeRange}
                </div>
                <div class="event-property">
                    ${event.location}
                </div>
            </div>
</#list>

<#if updated?has_content>
            <h1>Zmienione</h1>
</#if>
<#list updated as event>
            <div class="event-box updated">
                <div class="event-header">
                    ${event.summary}
                    <#if event.oldSummary?has_content>
                    <span class="change">
                     (wcześniej: ${event.oldSummary})
                    </span>
                    </#if>
                </div>
                <div class="event-property">
                    ${event.description}
                    <#if event.oldDescription?has_content>
                    <span class="change">
                     (wcześniej: ${event.oldDescription})
                    </span>
                    </#if>
                </div>
                <div class="event-property">
                    ${event.timeRange}
                    <#if event.oldTimeRange?has_content>
                    <span class="change">
                     (wcześniej: ${event.oldTimeRange})
                    </span>
                    </#if>
                </div>
                <div class="event-property">
                    ${event.location}
                    <#if event.oldLocation?has_content>
                    <span class="change">
                     (wcześniej: ${event.oldLocation})
                    </span>
                    </#if>
                </div>
            </div>
</#list>

        </div>
        <div class="footer">
            Wiadomość wygenerowana automatycznie
        </div>
    </div>
</div>
</body>
</html>
