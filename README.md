# WCY Schedule Updater - automatyczna synchronizacja planu zajęć
Prezentowane narzędzie zostało stworzone w celu umożliwienia automatycznej synchronizacji planu zajęć w e-dziekanacie WCY WAT z uslugą Google Calendar.
W przypadku wykrycia różnic pomiędzy planem zajęć we wskazanym kalendarzu Google (z podziałem na kalendarz wykładów i pozostałych zajęć):  
* aplikacja uaktualni kalendarz Google,
* wyśle mailowe powiadomienie o dokonaniu zmian.
## Urządzenia mobilne
![](readme-assets/mobile-day.png)
![](readme-assets/mobile-week.png)
![](readme-assets/mobile-month.png)
![](readme-assets/mobile-details.png)
## Przeglądarki WWW
![](readme-assets/browser-day.png)
![](readme-assets/browser-week.png)
![](readme-assets/browser-month.png)
![](readme-assets/browser-details.png)
# Konfiguracja - `config.json`
1. Wpisz swój identyfikator (`login`) i hasło (`password`) do konta w edziekanacie.
1. Wejdź na stronę ze swoim aktualnym rozkładem zajęć, skopiuj jej adres i wklej jako `url`.
    - Parametr `sid` nie będzie wykorzystany, ale nie musisz go usuwać z wklejanego adresu.
    - Potrzebne parametry to `mid`, `iid` oraz `exv`.
    - np. `https://s1.wcy.wat.edu.pl/ed/logged_inc.php?sid=458da025188f1a0a8f39bd47ef2b74e8&mid=328&iid=20184&exv=K7B1S4&pos=0&rdo=1&t=6801397`
1. Ustaw odpowiedni rok akademicki (`year`)
    - w formacie `yyyy/yyyy`
    - np. `2018/2019`
1. W polu `lectures_calendar` wklej identyfikator kalendarza [(dowiedz się więcej)](https://yabdab.zendesk.com/hc/en-us/articles/205945926-Find-Google-Calendar-ID), w którym chcesz zapisywać wykłady. Analogicznie w `others_calendar` identyfikator kalendarza do przechowywania pozostałych zajęć - laboratoriów, ćwiczeń, egzaminów itd.
    - jeśli chcesz, może być to ten sam identyfikator (nie będzie wtedy po prostu podziału na dwa kalendarze).
1. W polu `notification_email` wklej adres email, na który mają być wysyłane powiadomienia o zmianach (np. mail grupowy - [dowiedz się wiecej](https://groups.google.com/)).

# Pierwsze uruchomienie
Przy pierwszym uruchomieniu powinno być wymagane zalogowanie się na konto Google posiadające uprawnienia do modyfikowania kalendarzy wykorzystanych przy konfiguracji.  
![](readme-assets/consent-screen.png)

# Automatyczne aktualizacje planu
Do automatycznego uruchamiania programu można wykorzystać harmonogram zadań systemu Windows.  
Wykonanie poniższego kodu Powershell w folderze z plikiem `.jar` spowoduje dodanie zadania wykonywanego przy zalogowaniu i następnie co godzinę oraz o 22:30 każdego dnia. 
```powershell
# w $dir można podać bezpośrednią ścieżkę do folderu z plikiem jar
$dir = (Get-Item -Path ".\").FullName 

$action = New-ScheduledTaskAction -Execute 'javaw'  -Argument '-jar ".\wcy-schedule-updater-1.0.jar"' -WorkingDirectory $dir
$triggers = @()
$triggers +=  New-ScheduledTaskTrigger -Daily -At 22:30
$triggers +=  New-ScheduledTaskTrigger -AtLogOn
$task = Register-ScheduledTask -Action $action -Trigger $triggers -TaskName 'WCY Schedule Updater'

$task.Triggers[1].Repetition.Interval = "PT1H"
$task | Set-ScheduledTask
```
![](readme-assets/new-task-powershell.png)

# Samodzielna kompilacja projektu
> Jeśli chcesz samodzielnie skompilować aplikację, powinienieś najpierw wygenerować odpowiedni [projekt Google](https://console.developers.google.com) oraz [klucz klienta OAuth](https://developers.google.com/identity/protocols/OAuth2), który należy umieścić w lokalizacji `src/main/resources/credentials.json`.

Aby wygenerować odpowiedni plik `.jar` należy skorzystać z [Mavena](https://maven.apache.org/) i wywołać komendę:  
`mvn clean compile assembly:single`.
 
 Wygenerowany zostanie plik `.jar`, który uruchomiony w folderze z plikami `email-template.ftl` i odpowiednio uzupełnionym `config.json` będzie dokonywał aktualizacji planu zajęć.
 
